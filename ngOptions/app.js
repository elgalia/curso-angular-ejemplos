angular
    .module('myApp', [])
    .controller('MyController', function($scope){
        $scope.data = {
            availableOptions: [
                {id: '1', name: 'Option A', group: '1'},
                {id: '1.1', name: 'Option A 1', group: '1'},
                {id: '1.2', name: 'Option A 2', group: '1'},
                {id: '1.3', name: 'Option A 3', group: '1'},
                {id: '2', name: 'Option B', group: '2'},
                {id: '2.1', name: 'Option B 1', group: '2'},
                {id: '2.2', name: 'Option B 2', group: '2'},
                {id: '2.3', name: 'Option B 3', group: '2'},
                {id: '3', name: 'Option C', group: '3'}
           ],
           repeatSelect: null,
           selectedOption: {id: '3', name: 'Option C'}
        };
        $scope.set2ndOpt = function() {
            $scope.data.selectedOption = $scope.data.availableOptions[1];
        };
        $scope.setBogus = function() {
            $scope.data.selectedOption = $scope.data.repeatSelect = 'lalala';
        };
        var groupHash = {
            1: 'Group 1',
            2: 'Group 2',
            3: 'Group 3'
        };
        $scope.getGroupLabel = function(groupId) {    
            return groupHash[groupId];
        }
        
    });