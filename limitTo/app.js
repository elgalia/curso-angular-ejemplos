angular
    .module('myApp', [])
    .controller('MyController', function($scope){
        $scope.friends = [
          {name:'John', lastName: 'Malkovich', phone:'555-1276'},
          {name:'Mary', phone:'800-BIG-MARY'},
          {name:'Mike', phone:'555-4321'},
          {name:'Adam', phone:'555-5678'},
          {name:'Julie', phone:'555-8765'},
          {name:'Juliette', phone:'555-5678'},
          {name:'Wilson', phone:'555-9183'}
        ];
        
        //Pagination
        $scope.pageSize = 2;
        $scope.setPage = function($index) {
            $scope.offset = $index * $scope.pageSize;
        }
        
    });