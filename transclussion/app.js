angular
    .module('myApp', [])
    .controller('MyController', function($scope){
        $scope.title = 'Lorem Ipsum';
        $scope.text = 'Neque porro quisquam est qui dolorem ipsum quia dolor...';
    })
    .directive('pane', function(uppercaseFilter){
        
      return {
        restrict: 'E',
        transclude: true,
        scope: { title:'@' },
        template: '<div style="border: 1px solid black;">' +
                    '<div style="background-color: gray">{{title}}</div>' +
                    '<ng-transclude></ng-transclude>' +
                  '</div>'
        // , link: function(scope, element, attr, ctrl, transclude) {
        //     transclude(function(clone, scope) {
        //         element.children().append(clone).append('<br>');
        //     });
        //     var newScope = scope.$new();
        //     newScope.text = 'otro text';
        //     transclude(newScope, function(clone, scope) {
        //         element.children().append(clone);
        //     });
        // }
      };
  });