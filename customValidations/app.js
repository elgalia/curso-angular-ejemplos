angular
    .module('myApp', [])
    .directive('integer', function() {
        var INTEGER_REGEXP = /^\-?\d+$/;
        
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                ctrl.$validators.integer = function(modelValue, viewValue) {
                    // console.debug(modelValue, viewValue);
                    
                    if (ctrl.$isEmpty(modelValue)) {
                        // consider empty models to be valid
                        return true;
                    }

                    if (INTEGER_REGEXP.test(viewValue)) {
                        // it is valid
                        return true;
                    }

                    // it is invalid
                    return false;
                };
                /*ctrl.$parsers.push(function customParser(viewValue){
                  console.debug(viewValue);
                  return 1/100*viewValue;
                });
                  
                ctrl.$formatters.push(function customFormat(modelVal) {
                  return modelVal * 100;
                });*/
            }
        };
    })
    .directive('username', function($q, $timeout) {
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                var usernames = ['Jim', 'John', 'Jill', 'Jackie'];

                ctrl.$asyncValidators.username = function(modelValue, viewValue) {

                    if (ctrl.$isEmpty(modelValue)) {
                        // consider empty model valid
                        return $q.when();
                    }

                    var def = $q.defer();

                    $timeout(function() {
                        // Mock a delayed response
                        if (usernames.indexOf(modelValue) === -1) {
                            // The username is available
                            def.resolve();
                        } else {
                            def.reject();
                        }

                    }, 2000);

                    return def.promise;
                };
            }
        };
        });