angular
    .module('myApp', [])
    .directive('myTabs', function() {
        return {
            restrict: 'E',
            transclude: true,
            scope: {},
            controller: ['$scope', function($scope) {
                var panes = $scope.panes = [];

                $scope.select = function(pane) {
                    angular.forEach(panes, function(pane) {
                        pane.selected = false;
                    });
                    pane.selected = true;
                };

                this.addPane = function(pane) {
                    if (panes.length === 0) {
                        $scope.select(pane);
                    }
                    panes.push(pane);
                };
            }],
            template: 
                '<div class="tabbable">\n'+
                    '<ul class="nav nav-tabs">\n'+
                        '<li ng-repeat="pane in panes" ng-class="{active:pane.selected}">\n'+
                            '<a href="" ng-click="select(pane)">{{pane.title}}</a>\n'+
                        '</li>\n'+
                    '</ul>\n'+
                    '<div class="tab-content" ng-transclude>\n'+
                    '</div>\n'+
                '</div>'
        };
    })
    .directive('myPane', function() {
        return {
            require: '^myTabs',
            restrict: 'E',
            transclude: true,
            scope: {
                title: '@'
            },
            link: function(scope, element, attrs, tabsCtrl) {
                tabsCtrl.addPane(scope);
            },
            template: 
                '<div class="tab-pane" ng-show="selected" ng-transclude>\n'+
                '</div>'
        };
    });