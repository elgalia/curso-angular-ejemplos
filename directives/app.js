angular
    .module('myApp', [])
    .controller('MyController', function($scope) {
        $scope.persona = {
            nombre: "Esteban"    
        };
    })
    .directive('helloWorld', function() {
        
        return {
            templateUrl: 'hello-world.html',
            controller: MyDirectiveController,
            controllerAs: 'myDirCtrl'
        };
        
        function MyDirectiveController($scope) {
            this.apellido = 'Galianoooo';
        }
        
    });