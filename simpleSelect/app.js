angular
    .module('myApp', [])
    .controller('MyController', function($scope){
        $scope.data = {
            singleSelect: null,
            multipleSelect: [],
            option1: 'option-1',
        };

        $scope.forceUnknownOption = function() {
            $scope.data.singleSelect = 'nonsense';
        };
    });