angular
    .module('myApp', [])
    .controller('MyController', function($scope){
        $scope.users = [
            { name: "Mary", points: 310 },
            { name: "June", points: 290 },
            { name: "Bob", points: 300}
        ];
        $scope.changeUsers = function() {
            $scope.users = [
                { name: "María", points: 310 },
                { name: "Julia (?)", points: 290 },
                { name: "Boby", points: 300}
            ];
        };
        $scope.pushUser = function() {
            $scope.users.push({ name: "Nuevo usuario" });
        };
        $scope.modifyUser = function() {
            $scope.users[0].name = "Nombre cambiado";
        }
    })
    .controller('SimpleWatch', function($scope) {
        var vm = this;
        
        $scope.$watch('users', function(value, previous) {
            vm.users = angular.copy(value, []);
            console.debug('simple watch fired');
        });
    })
    .controller('WatchCollection', function($scope) {
        var vm = this;
        
        $scope.$watchCollection('users', function(value, previous) {
            vm.users = angular.copy(value, []);
            console.debug('collection watch fired');
        });
    })
    .controller('DeepWatch', function($scope) {
        var vm = this;
        
        $scope.$watch('users', function(value, previous) {
            vm.users = angular.copy(value, []);
            console.debug('deep watch fired');
        }, true);
    })
    ;