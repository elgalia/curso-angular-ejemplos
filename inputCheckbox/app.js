angular
    .module('myApp', [])
    .controller('MyController', function($scope) {
        $scope.checkboxModel = {
            value1: true,
            value2: 'YES'
        };
    });