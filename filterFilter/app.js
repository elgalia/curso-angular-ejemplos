angular
    .module('myApp', [])
    .controller('MyController', function($scope){
        $scope.friends = [
          {name:'John', lastName: 'Malkovich', phone:'555-1276'},
          {name:'John', lastName: 'Bonachon', phone:'555-1277'},
          {name:'Mary', phone:'800-BIG-MARY'},
          {name:'Mike', phone:'555-4321'},
          {name:'Adam', phone:'555-5678'},
          {name:'Julie', phone:'555-8765'},
          {name:'Juliette', phone:'555-5678', parents: {
              mother: 'Juliana',
              father: 'Raul'
          }},
          {name:'Jacinta', phone:'555-9183', parents: {
              mother: 'Elsa', father: 'Herminio'
          }}
        ];
        
        $scope.nameOrParent = function(value, index, array) {
            var include = false;
            
            if (!$scope.name && !$scope.parentName)
                include = true;
                
            else if (value.name.indexOf($scope.name) > -1)
                include = true;
                
            else if (value.parents)
                include = value.parents.mother.indexOf($scope.parentName) > -1 || 
                          value.parents.father.indexOf($scope.parentName) > -1;
                       
            return include;    
        };
        
        $scope.comparator = function(actual, expected) {
            
            if (typeof(actual) === 'string') {
                 return actual.indexOf(expected) > -1 ||
                        actual.split('').reverse().join('').indexOf(expected) > -1;
            }
                
            return false;
        }
        
    });