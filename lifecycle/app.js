angular
    .module('myApp', [])
    
    .config(function() {
        console.log("BOOTSTRAP PHASE STARTED");
        console.log("app config");
    })
    
    .run(function() {
        console.log("app run");
    })
    
    .controller('MyController', function(/*aProvider*/){
        console.log("app controller");
    })
    
    .factory('aProvider', function() {
        console.log("factory");
        return {};
    })
    
    .directive("test1", function() {
        console.log("COMPILE PHASE STARTED");
        console.log("directive setup");
        return {
            compile: function() {
                console.log("directive compile");
            }
        }
    })
    
    .directive("test2", function() {
        return {
            link: {
                pre: function() {
                    console.log("directive pre-link");
                },
                post: function() {
                    console.log("directive post-link");
                }
            }
        }
    })
    
    ;