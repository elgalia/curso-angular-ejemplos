# Curso AngularJS - Ejemplos
En este repo encontrarán algunos de los ejemplos utilizados durante el transcurso del curso. La idea es que los utilicen como ayuda memoria en caso de no acordarse alguna explicación. Los ejemplos más complicados tienen algunar partes del código comentado, para mostrar diferentes formas de obtener similares resultados. La idea es que prueben descomentando, modificando y observando cómo cambia el resultado.

## Dependencias
Necesitan tener instalados:

- obviamente **git** para poder clonar este repo y explorarlo ([guía](https://git-scm.com/book/es/v1/Empezando-Instalando-Git))
- **node/npm**, para instalar las dependencias y ejecutar algunos tasks interesantes ([guía](https://nodejs.org/en/download/package-manager/#windows))

## Instalación
En la consola ejecuten...

```bash
git clone git@bitbucket.org:elgalia/curso-angular-ejemplos.git
cd curso-angular-ejemplos
npm install
```

# Guía de uso
Para arrancar la aplicación visiten `localhost:8085`, habiendo corrido previamente

```bash
npm start
```