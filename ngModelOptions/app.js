angular
    .module('myApp', [])
    .controller('MyController', function($scope){
        $scope.user = {
            name: 'John',
            data: '',
            wrappedData: wrappedData
        };

        $scope.cancel = function(e) {
            if (e.keyCode == 27) { //Esc
                $scope.userForm.userName.$rollbackViewValue();
            }
        };

        var _data;

        function wrappedData(data) {
            if (arguments.length)
                _data = data;
            else
                return _data;
        }
    });