angular
    .module('myApp', [])
    .controller('MyController', function($scope){
        $scope.color = {
            name: 'blue'
        };
        $scope.value = {
            name: 'blue',
            label: "Blue"
        };
        $scope.specialValue = {
            "id": "12345",
            "value": "green",
            "label": "Green"
        };
    });